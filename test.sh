if git rev-parse v2.0 >/dev/null 2>&1
then
    echo "Found tag"
    exit 0
else
    echo "Tag not found"
    exit 1
fi
